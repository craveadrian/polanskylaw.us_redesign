	<div id="content">
		<div class="row">
			<h1>Divorce Attorney & Family Law Attorney</h1>
			<p>Review the general information to help determine if you need legal assistance. If you wish to discuss the matter further, please call to schedule an initial office consultation.</p>
			<p>In a divorce, the Court will end the marriage between the parties and dispose of property issues. If children were born or adopted during the marriage, the Court issues orders concerning their conservatorship (custody), child support, visitation, health insurance, etc. </p>
			<p>Each divorce is unique. Some couples are able to reach an agreement on property issues and issues regarding their children without a large amount of intervention. Other cases require discovery which involves the production of documents and other types of information. This is especially true when one party may be less than forthcoming regarding financial matters or certain behaviors. Discovery methods are used to obtain a wide range of information such as tax returns, bank records, deeds and vehicle titles.</p>
			<p>After sufficient information is obtained regarding the issues in the divorce proceeding, the parties should attempt to resolve their case. Most divorce cases are settled through direct negotiation between the parties’ attorneys, or through mediation. This is due to the significant financial costs to each party to prepare for Trial on the Merits. In some cases, Trial on the Merits is necessary and The Polansky Law Firm, PLLC will work tirelessly to litigate your case in court to obtain the best possible outcome for you. In Texas, the Court uses a "just and right" division with regard to division of community property. Decisions regarding children are based on the "best interests" of the children.</p>
			<p>Call the office today to consult with the attorney regarding your specific circumstances.</p>
		</div>
	</div>
