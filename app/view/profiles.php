	<div id="content">
		<div class="row">
			<h1 class="atName">Attorney Serving Greater Brazosport Area including Matagorda, Galveston and Wharton Counties</h1>
			<div class="attleft inb">
				<img src="public/images/content/attorney/attorney1.jpg" alt="Attorney Christine A. Polansky">
			</div>
			<div class="attright inb">
				<h4>Education:</h4>
				<ul>
					<li> <p> <span>University of Houston Law Center</span>Doctor of Jurisprudence  </p> </li>
					<li> <p> <span>Texas A&M University</span> Doctor of Philosophy - Chemistry  </p> </li>
					<li> <p> <span>University of Pittsburgh</span>Bachelor of Science - Chemistry, Magna Cum Laude  </p> </li>
				</ul>
				<h4>Licensed to Practice:</h4>
				<ul>
					<li> <p>Supreme Court of Texas, All State Courts in Texas</p></li>
					<li> <p>United States District Court of Southern Texas</p></li>
					<li> <p>United States Bankruptcy Court - Southern District of Texas</p></li>
					<li> <p>United States Patent and Trademark Office</p></li>
				</ul>
			</div>
			<p class="atDesc">The Polansky Law Firm, PLLC, provides legal representation to the residents of Lake Jackson, Clute, Freeport, Brazoria, Angleton, Bay City and surrounding areas. Based in the Greater Brazosport Area, the Polansky Law Firm, PLLC, practices law in Brazoria, Matagorda, Fort Bend, Galveston, Wharton and Harris Counties. When you are facing tough legal issues call the Polansky Law Firm, PLLC , in Lake Jackson for representation in bankruptcy, contract and property disputes, divorce, family law, wills and probate.</p>
		</div>
	</div>
