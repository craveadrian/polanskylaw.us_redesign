	<div id="content">
		<div class="row">
			<h1>Bankruptcy Lawyer Serving Brazoria, Fort Bend, Galveston, Matagorda, Wharton and Harris Counties</h1>
			<div id="chapter7" class="laws">
				<h3>Chapter 7 - Liquidation or Straight Bankruptcy</h3>
				<p>In a typical Chapter 7 bankruptcy the debtor does not attempt to pay back unsecured creditors, such as credit card companies. It is designed for debtors who do not have the ability to pay even a portion of those debts. Debtors whose debts are primarily consumer debts are subject to a "means test" to determine whether they are permitted to file bankruptcy under Chapter 7. The federal government determines the median income for your geographical region and family size. If your income is greater than the median income for your region and family size you may not be permitted to file bankruptcy under Chapter 7.</p>
				<p>A Chapter 7 bankruptcy will result in the discharge of certain debts. Debts that may be eliminated are credit card debt, medical bills, unsecured loans, lines of credit, foreclosure deficiencies, repossession deficiencies and most judgments. The debtor will still be responsible for most taxes, student loans, child support, property settlement obligations and other exceptions. Please call our office to consult with an attorney regarding your specific circumstances.</p>
				<p>If the debtor is current on their home and/or car payments they may be able to "reaffirm" the debts and continue to make the payments and retain the property.</p>
				<p>A Chapter 7 bankruptcy is not designed for the debtor who has a mortgage arrearage, or vehicle arrearage and they still want to keep the home or vehicle.  It does not contain a mechanism for the debtor to “catch up” on their payments.  </p>
			</div>
			<div id="chapter13" class="laws">
				<h3>Chapter 13 - Wage Earner's Plan</h3>
				<p>The Chapter 13 bankruptcy differs from the Chapter 7 bankruptcy in that the debtor agrees to pay a portion or all of their debts over a period of three or five years.  The amount that the debtor pays is specific to each case.  This is referred to as a Payment Plan and is designed for debtors with regular income, or have arrearages on their home or vehicles. Most Payment Plans are five years.  The Court must approve the plan before it can take effect. The debts are discharged after completion of the plan. However, as in a Chapter 7 bankruptcy certain debts are not dischargeable.</p>
				<p>One advantage of the Chapter 13 bankruptcy is that it may be possible for the debtor(s) to make up an arrearage on their home mortgage or vehicles during the life of the plan, thus preventing a foreclosure on the home or repossession of vehicles or other items secured by a loan.</p>
				<p>Call for an initial consultation if you are considering filing for bankruptcy to obtain relief from your creditors.</p>
			</div>
		</div>
	</div>
