<div id="content">
	<div class="row">
		<div class="services-category">
			<section>
				<h2>Bankruptcy<br>
				Lawyer</h2>
				<br>

				<p>As a bankruptcy attorney, Ms. Polansky has helped clients stop foreclosure proceedings and has helped them prevent the repossession of vehicles and other belongings. As a bankruptcy attorney serving Brazoria, Matagorda, Fort Bend, Wharton, Harris, and Galveston Counties, Ms. Polansky is able to analyze your financial situation and recommend the best course of action for your situation.</p>
				<br>

				<p><?php $this->info("company_name"); ?> is a debt relief agency. The firm helps people file for bankruptcy debt relief under the bankruptcy code.</p>
			</section>
			<section>
				<h2>Divorce Lawyer and <span>Family Law Attorney</span></h2>
				<br>

				<p>The Polansky Law Firm, PLLC works hard to represent your interests in cases of divorce, primary residence of the children, custody issues, parental rights, child support, visitation rights, community property, separate property, prenuptial agreements, enforcements, modifications and other issues. Primary residence of the children and community property interests are extremely important issues in Family Law. You deserve an experienced attorney that will protect your rights as a parent and as a spouse.</p>
			</section>

			<section>
				<h2>Civil<br>
				Lawyer</h2>
				<br>

				<p>Contract disputes over good and services and property are very common. Such disputes include disputes with contractors or for services that were not received and property damage to name just a few. Ms. Polansky is an experienced litigator who can represent you in litigation, negotiations, mediations and settlements in these areas.</p>
			</section>

			<section>
				<h2>Wills and <span>Probate Attorney<!--, Guardianship Attorney--></span></h2>
				<br>

				<p>With a focus on wills and probate, the firm can also draft Statutory Durable Powers of Attorney, Medical Powers of Attorney and Living Wills or Directives to Physicians.</p>
				<br>

				<!--<p>As a lawyer, I am sensitive to your concerns regarding guardianship. I will work hard to make the process of filing a guardianship case less stressful. <?php $this->info("company_name"); ?> assists families in guardianship for minors, guardianship for disabled adults and estate planning issues.</p>-->
			</section>
		</div>

		<div class="form-section">
			<a href="tel:979-266-9281"><p><small>Call us Today! (979) 266-9281</small></p></a>

			<form action="sendContactForm" method="post"  class="sends-email ctc-form">
				<h2>How can we help you?</h2>
				<div class="sub-container">
					<div class="left-input">
						<label><span>Name</span>
							<input type="text" name="name">
						</label>

						<label><span>Email</span>
							<input type="text" name="email">
						</label>

						<label><span>Phone</span>
							<input type="text" name="phone">
						</label>
					</div>

					<div class="right-input">
						<label><span>A Brief description of your legal needs</span>
							<textarea name="message"></textarea>
						</label>

						<div class="cap-cover">
							<label for="g-recaptcha-response"></label>
							<div class="g-recaptcha"></div>
						</div>
					</div>

					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label><br>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
