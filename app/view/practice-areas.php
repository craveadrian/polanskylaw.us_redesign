<div id="content">
  <div class="row">
    <h1>Practice Areas</h1>
    <div class="laws">
      <a href="<?php echo URL ?>chapter7and13#chapter7"><h2>Bankruptcy</h2></a>
      <ul>
        <li> <p class="pa-list"> Chapter 7 Liquidation Bankruptcy or Straight Bankruptcy</p> </li>
        <li> <p class="pa-list"> Chapter 13 Wage Earner's Plan Bankruptcy</p> </li>
      </ul>
    </div>
    <div class="laws">
      <a href="<?php echo URL ?>divorce-and-family"><h2>Family Law</h2></a>
      <ul>
        <li> <p class="pa-list">Divorce</p> </li>
        <li> <p class="pa-list">Adoption and/or Termination</p> </li>
        <li> <p class="pa-list">Annulment</p> </li>
        <li> <p class="pa-list">Primary Residence of Children, Custody Disputes or Visitation</p> </li>
        <li> <p class="pa-list">Child Support</p> </li>
        <li> <p class="pa-list">Contempt</p> </li>
        <li> <p class="pa-list">Division of Property, Community Property, Separate Property</p> </li>
        <li> <p class="pa-list">Grandparent's Rights</p> </li>
        <li> <p class="pa-list">Modification of Support or Custody</p> </li>
        <li> <p class="pa-list">Paternity</p> </li>
        <li> <p class="pa-list">Restraining Orders</p> </li>
        <li> <p class="pa-list">Spousal Maintenance or Alimony</p> </li>
      </ul>
    </div>
    <div class="laws">
      <a href="<?php echo URL ?>civil"><h2>Civil Law</h2></a>
      <ul>
        <li> <p class="pa-list">Consumer Law</p> </li>
        <li> <p class="pa-list">Deceptive Trade Practices</p> </li>
        <li> <p class="pa-list">Contract Disputes</p> </li>
        <li> <p class="pa-list">Debt Collection</p> </li>
        <li> <p class="pa-list">Estate Planning - Wills and Probate</p> </li>
        <ul>
          <li> <p class="pa-list">Financial Powers of Attorney, Health Care Powers of Attorney and Living Wills</p> </li>
          <li> <p class="pa-list">Wills</p> </li>
          <li> <p class="pa-list">Probate</p> </li>
        </ul>
        <li> <p class="pa-list">Insurance Claims/Disputes</p> </li>
        <li> <p class="pa-list">Real Estate</p> </li>
        <ul>
          <li> <p class="pa-list">Drafting of Deeds and other Documents</p> </li>
        </ul>
      </ul>
    </div>
  </div>
</div>
