<div id="content">
	<div class="row">
		<div class="cntTop">
			<p class="cntText inb">Addess: <small><?php $this->info("address"); ?></small></p>
			<p class="cntText inb">Phone: <small><?php $this->info(["phone","tel"]); ?></small></p>
			<p class="cntText inb">Fax: <small><?php $this->info(["fax","tel"]); ?></small></p>
		</div>
		<div class="cntBot">
			<div style="width: 100%"><iframe width="100%" height="300" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=107%20West%20Way%2C%20Suite%2015%20Lake%20Jackson%2C%20TX%2077566+(The%20Polansky%20Law%20Firm%2C%20PLLC)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google Maps iframe generator</a></iframe></div><br />
		</div>
		<h1>Contact Us</h1>
		<br>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span>Name:</span>
				<input type="text" name="name">
			</label>
			<label><span>Address:</span>
				<input type="text" name="address">
			</label>
			<label><span>Email:</span>
				<input type="text" name="email">
			</label>
			<label><span>Phone:</span>
				<input type="text" name="phone">
			</label>
			<label><span>Message:</span>
				<textarea name="message"></textarea>
			</label>
			<div class="cap-cover">
				<label for="g-recaptcha-response"></label>
				<div class="g-recaptcha"></div>
			</div>
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label><br>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>Submit Form</button>
		</form>
	</div>
</div>
