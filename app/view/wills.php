	<div id="content">
		<div class="row">
			<h1>Wills &amp; Probate</h1>
			<div class="laws">
				<h3>Last Will and Testament Attorney</h3>
				<p>A Will directs the distribution of your estate upon your death.   In order for a Will to take effect it must be probated after your death.  Many parents have Wills which contain a Contingent Trust clause. The Contingent Trust only becomes active after the death of both parents. The Contingent Trust clause names a Trustee to manage the children's assets until they reach an age specified in the Will.  A separate trust document is not necessary in these cases.</p>
				<p>In Texas, if you pass away without a Will, the distribution of your assets will be determined by the Texas Estate Code. Factors affecting the distribution of assets are the nature of the property (community property or separate property) and the nature of your family (spouse, children from one or more relationships, no children, siblings, parents). If you are married, do not assume your spouse will automatically receive your entire interest in real estate or other assets.  Generally speaking, it is more expensive to probate an intestate estate (where the decedent did not have a Will) than a testate estate.  Having a Will makes it easier on your beneficiaries in more ways than one.</p>
				<p>A thorough discussion of your needs and your wishes with The Polansky Law Firm, PLLC will ensure that your property will be divided according to your specifications.</p>
			</div>
			<div class="laws">
				<h3>Documents Needed in the Event of Incapacity (Prior to Death)</h3>
				<p><u>Durable Power of Attorney</u>: This permits you to name whom you trust to handle your financial matters, while you are living, if you are out-of-state or overseas, temporarily or permanently incompetent.  In Texas, the Durable Power of Attorney is typically filed with the County Clerk, the original is returned to the client after filing with the Clerk’s office.  It is important to complete the power of attorney BEFORE any competency issues arise; doing so can prevent the need for a Guardianship later in life.</p>
				<p><u>Living Will or Directive to Physician</u>: This directs under very limited circumstances when a person's life should or should not be artificially prolonged by extraordinary measures.</p>
				<p><u>Medical Power of Attorney</u>: This only becomes active in the event of your incapacity. If you are incapacitated the physician is then required to discuss health care issues with the person you appointed.</p>
			</div>
			<div class="laws">
				<h3>Trust Attorney</h3>
				<p>Trusts can be used to minimize estate taxes, provide for disabled children, your own incapacity, education, retirement and for other purposes. Many people choose a Revocable Living Trust to minimize probate after they pass away.  However, a wisely drafted Will and current beneficiary designations on bank and investment accounts can also minimize probate.  Texas is typically referred to as a “friendly” probate state so a Revocable Living Trust is not necessary for everyone.  The most common mistake made by individuals regarding Revocable Living Trusts is the failure to change ownership of real property over to the Trust.  A Revocable Living Trust may or may not be the right choice for you.</p>
				<p>If you have questions regarding whether a Revocable Living Trust may be best for your particular circumstances please call the office to schedule a free initial confidential consultation.</p>
			</div>
			<div class="laws">
				<h3>Probate Attorney</h3>
				<p>The probate process is the administration of the decedent’s estate.  It is done with Court approval.  The complexity of the probate process depends on the size and complexity of the estate. Texas has "independent administration" and a properly drafted Will can simplify the probate process by qualifying for independent administration.  In many independent administrations one brief hearing is all that the Court requires.  The benefits of a property drafted Will cannot be over stressed.</p>
				<p>In contested cases, where there is conflict between the beneficiaries a “dependent administration” is typically required.  A dependent administration does require more input from the Court.</p>
			</div>
		</div>
	</div>
