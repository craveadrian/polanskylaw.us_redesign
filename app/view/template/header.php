<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
<header>
	<div class="row">
		<a href="<?php echo URL ?>"><img src="public/images/home-icon.png" alt="Home Icon" class="home-icon"></a>
		<nav>
			<a href="#" id="pull">Menu</a>
			<ul>
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
				<li <?php $this->helpers->isActiveMenu("profiles"); ?>><a href="<?php echo URL ?>profiles">ATTORNEY</a></li>
				<li><a href="<?php echo URL ?>practice-areas">Practice Areas</a>
					<ul class="sub-nav">
						<li <?php $this->helpers->isActiveMenu("chapter7and13"); ?>><a href="<?php echo URL ?>chapter7and13#chapter7">Bankruptcy Chapter 7</a></li>
						<li <?php $this->helpers->isActiveMenu("chapter7and13"); ?>><a href="<?php echo URL ?>chapter7and13#chapter13">Bankruptcy Chapter 13</a></li>
						<li <?php $this->helpers->isActiveMenu("divorce-and-family"); ?>><a href="<?php echo URL ?>divorce-and-family">Divorce & Family Law</a></li>
						<li <?php $this->helpers->isActiveMenu("civil"); ?>><a href="<?php echo URL ?>civil">Civil Law & Contract Disputes</a></li>
						<li <?php $this->helpers->isActiveMenu("wills"); ?>><a href="<?php echo URL ?>wills">Wills & Probate</a></li>
							<!-- <li <?php //$this->helpers->isActiveMenu("commercial-attorney-and-business-lawyer"); ?>><a href="<?php //echo URL ?>commercial-attorney-and-business-lawyer">Commercial Attorney & Business Lawyer</a></li> -->
					</ul></li>
				<li <?php $this->helpers->isActiveMenu("resources"); ?>><a href="<?php echo URL ?>resources">Resources</a></li>
				<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">Contact Us</a></li>

			</ul>
		</nav>
	</div>
</header>

<?php if($view == "home"):?>
<!-- banner -->
<div id="banner">
	<div class="bannerTop">
		<div class="logo-text">
			<div class="row">
				<!-- company logo -->
				<p class="logo"><a href="home"><img src="public/images/common/logo.png" alt=""></a></p>
				<!-- end -->

				<div class="number-text">
					<p>Call us for a <small>Free</small><br>initial consultation</p>
					<p><?php $this->info(["phone","tel"]); ?></p>
				</div>
				</div>
			</div>

		<div class="row">
			<p class="first-image"><img src="public/images/common/banner.jpg" alt=""></p>
			<div class="call-text">
				<a href="tel:979-266-9281"><p>CALL US TODAY!</p></a>
			</div>
		</div>
	</div>
	<div class="bannerBot">
		<div class="row">
			<div class="services-section">
				<p><?php $this->info("company_name"); ?> provides legal services for Lake Jackson, Clute, Freeport, Brazoria, Angleton and surrounding cities. Based in the Greater Brazosport Area, <?php $this->info("company_name"); ?> practices law in Brazoria County, Fort Bend County, Galveston County, Matagorda County, Harris County and Wharton county. Call to schedule an initial consultation.</p>

				<dl>
					<dt><a href="<?php echo URL ?>chapter7and13#chapter7"><img src="public/images/content/services-dt1.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>chapter7and13#chapter7">Bankruptcy Chapter 7</a></dd>
				</dl>

				<dl>
					<dt><a href="<?php echo URL ?>chapter7and13#chapter13"><img src="public/images/content/services-dt2.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>chapter7and13#chapter13">Bankruptcy Chapter 13 </a></dd>
				</dl>

				<dl>
					<dt><a href="<?php echo URL ?>divorce-and-family"><img src="public/images/content/services-dt3.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>divorce-and-family">Divorce</a></dd>
				</dl>

				<dl>
					<dt><a href="<?php echo URL ?>divorce-and-family"><img src="public/images/content/services-dt4.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>divorce-and-family">Family Law</a></dd>
				</dl>

				<dl>
					<dt><a href="<?php echo URL ?>civil"><img src="public/images/content/services-dt5.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>civil">Civil Law &amp; Contract Disputes </a></dd>
				</dl>

				<dl>
					<dt><a href="<?php echo URL ?>wills"><img src="public/images/content/services-dt6.jpg" alt=""></a></dt>
					<dd><a href="<?php echo URL ?>wills">Wills &amp; Probate</a></dd>
				</dl>
			</div>
		</div>
	</div>
</div>
<!-- end -->
<?php endif; ?>
