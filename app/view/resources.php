	<div id="content">
		<div class="row">
			<h1>Resources</h1>
			<h3>Links</h3>
			<p class="resources"><strong>Bankruptcy</strong>
				<span> <a href="https://www.uscourts.gov/bankruptcycourts/bankruptcybasics.html" target="_blank">www.uscourts.gov/bankruptcycourts/bankruptcybasics.html</a> </span>
			</p>
			<p class="resources"><strong>Texas Constitution and Statutes (Family Code, Property Code and Probate code)</strong>
				<span> <a href="https://www.statutes.legis.state.tx.us" target="_blank">www.statutes.legis.state.tx.us</a> </span>
			</p>
			<p class="resources"><strong>Texas Office of Attorney General Child Support</strong>
				<span> <a href="https://www.oag.state.tx.us/cs/" target="_blank">www.oag.state.tx.us/cs/</a> </span>
			</p>
			<p class="resources"><strong>State Bar of Texas</strong>
				<span> <a href="https://www.texasbar.com" target="_blank">www.texasbar.com</a> </span>
			</p>
			<p class="resources"><strong>Brazoria County (County Clerk and District Clerk)</strong>
				<span> <a href="https://www.brazoria-county.com" target="_blank">www.brazoria-county.com</a> </span>
			</p>
			<p class="resources"><strong>Brazoria County Appraisal District</strong>
				<span> <a href="https://www.brazoriacad.org" target="_blank">www.brazoriacad.org</a> </span>
			</p>
			<p class="resources"><strong>Guardianship</strong>
				<span> <a href="https://www.brazoriacountytx.gov/departments/guardianship-office" target="_blank">www.brazoriacountytx.gov/departments/guardianship-office</a> </span>
			</p>
			<p class="resources"><strong>www.publicbrazoria-county.com/PublicAccess/default.aspx</strong>
				<span> <a href="https://www.publicbrazoria-county.com/PublicAccess/default.aspx" target="_blank">www.publicbrazoria-county.com/PublicAccess/default.aspx</a> </span>
			</p>
		</div>
	</div>
